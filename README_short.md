# Stack (python)

Set of programming assignments that are designed to test knowledge of stack data structure.

## Problem 1: Implement stack using the default interface

Your first programming assignment is to implement the provided default interface for Stack.

## Problem 2: Check whether a given parentheses expression is valid

Given a string `s` containing just the characters `'('`, `')'`, `'{'`, `'}'`, `'['` and `']'`, determine if the input string is valid.

## Problem 3: Stack with minimum operation

Let's extend the default interface of Stack data structure and add a support of `get_minimum` method.

## Problem 4: Reverse Polish notation parsing

Your task is to evaluate the value of an arithmetic expression in [Reverse Polish Notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation).
