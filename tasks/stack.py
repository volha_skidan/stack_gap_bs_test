"""Templates for programming assignments: stack API."""
from typing import Any, Optional


class Stack:
    """Default interface for Stack data structure."""

    def __init__(self):
        self.stack = []

    def empty(self) -> bool:
        """Returns True if the stack is empty.

        NOTE: O(1) complexity is expected for this operation.
        """
        return len(self.stack) == 0

    def size(self) -> int:
        """Returns the number of elements within the stack.

        NOTE: O(1) complexity is expected for this operation.
        """
        return len(self.stack)

    def push(self, element: Any):
        """Adds a given element to the top of the stack.

        NOTE: O(1) complexity is expected for this operation.
        """
        self.stack.append(element)

    def pop(self) -> Any:
        """Returns the top element and removes it.

        NOTE: O(1) complexity is expected for this operation.

        Raises:
            ValueError: If the stack is empty.
        """
        if self.empty():
            raise ValueError
        else:
            return self.stack.pop()

    def peak(self) -> Any:
        """Returns the top element.

        NOTE: O(1) complexity is expected for this operation.

        Raises:
            ValueError: If the stack is empty.
        """
        if self.empty():
            raise ValueError
        else:
            return self.stack[-1]


class StackWithMinimum(Stack):
    """Extended Stack class that supports minimum operation.

    Assume that elements of stack are numerical (so that minimum operation is eligible).
    """

    def __init__(self):
        super().__init__()
        self.min = []

    def get_minimum(self) -> Optional[int]:
        """Returns the minimum element in the stack.

        NOTE: if the stack is empty - returns None.
        NOTE: O(1) complexity is expected for this operation.
        """
        if self.empty():
            return None
        return self.min[-1]

    def push(self, element: Any):
        """Adds a given element to the top of the stack.

        NOTE: O(1) complexity is expected for this operation.
        """
        if self.size() == 0:
            self.min.append(element)
        else:
            self.min.append(min(self.min[-1], element))
        self.stack.append(element)

    def pop(self) -> Any:
        """Returns the top element and removes it.

        NOTE: O(1) complexity is expected for this operation.

        Raises:
            ValueError: If the stack is empty.
        """
        if self.empty():
            raise ValueError
        else:
            self.min.pop()
            return self.stack.pop()