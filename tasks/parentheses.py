"""Template for programming assignment: parentheses validation."""


def is_valid_parentheses(expression: str) -> bool:
    """Returns True if a given string is valid parentheses expression.

    Args:
        expression: String, input string for validation.
    Returns:
        Boolean, whether a given string is valid parentheses expression.
    """
    parenthesis = {'[': ']', '{': '}', '(': ')'}
    st = []
    for ch in expression:
        if ch in parenthesis:
            st.append(ch)
        else:
            if st:
                prev_ch = st.pop()
            else:
                return False
            if ch != parenthesis[prev_ch]:
                return False
    return len(st) == 0

