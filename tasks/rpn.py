"""Template for programming assignment: Reverse Polish notation."""
from typing import List


def evaluate_rpn_tokens(rpn_tokens: List[str]) -> int:
    """Returns the evaluation result of a given list of RPN tokens."""
    st = []
    operations = '+-*/'
    for el in rpn_tokens:
        if el in operations:
            num1 = st.pop()
            num2 = st.pop()
            if el == '+':
                st.append(num1+num2)
            elif el == '-':
                st.append(num2 - num1)
            elif el == '*':
                st.append(num1 * num2)
            else:
                st.append(int(num2 / num1))
        else:
            st.append(int(el))
    return st.pop()
